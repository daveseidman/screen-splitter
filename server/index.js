const express = require('express');

const app = express();
const http = require('http');

const server = http.createServer(app);
const { Server } = require('socket.io');

const io = new Server(server);
const ip = require('ip');

const port = 8000;

const rooms = [];

const createID = (length) => {
  let result = '';
  const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
  for (let i = 0; i < length; i += 1) {
    result += characters.charAt(Math.floor(Math.random() * characters.length));
  }
  return result;
};

const createRoom = () => {
  let room = createID(4);
  while (rooms.indexOf(room) >= 0) room = createID(4);
  return room;
};

app.get('/', (req, res) => {
  res.sendFile(`${__dirname}/index.html`);
});

io.on('connection', (socket) => {
  const room = createRoom();
  rooms.push(room);
  console.log('new connection', room, 'total rooms', rooms.length);
  // socket.emit('ip', `http://${ip.address()}/${room}`);
  socket.emit('location', { ip: ip.address(), port, room });

  socket.on('disconnect', (e) => {
    console.log('disconnected from', room);
    rooms.splice(rooms.indexOf(room), 1);
  });
});

server.listen(port, () => {
  console.log('server listening on port 8000');
});
