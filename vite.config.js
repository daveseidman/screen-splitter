import { defineConfig } from 'vite';
import react from '@vitejs/plugin-react';
import mkcert from 'vite-plugin-mkcert';

export default defineConfig(({ command, mode, ssrBuild }) => {
  const config = {
    plugins: [react()],
    server: {
      port: 8080,
      host: true,
      proxy: {
        '/socket.io': 'http://localhost:8000',
      },
    },
    assetsInclude: ['**/*.dat', '**/*.patt'],
  };

  if (mode === 'https') {
    config.plugins.push(mkcert());
  }
  return config;
});
