// TODO: consider renaming / combining this file with main.jsx;
import create from 'zustand';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';


const useStore = create((set, get) => ({
  connected: false,
  host: false,
  socket: null,

  socketConnected: (socket) => {
    set(() => ({ socket }))
  },
  updateProgress: (amount) => {
    set(() => ({ progress: amount }));
  },


  setObject: (type, id) => {

    set(() => ({ [type]: null, loading: false, edited: false }));
  }
}));

export default useStore;
