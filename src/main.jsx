import React from 'react';
import ReactDOM from 'react-dom/client';
import { mountStoreDevtool } from 'simple-zustand-devtools';
import useStore from './store';
import App from './App';

ReactDOM.createRoot(document.getElementById('root')).render(
  <App />,
);
mountStoreDevtool('Store', useStore);