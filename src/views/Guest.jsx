import React, { useEffect, useRef } from 'react';
import './Guest.scss';
import testImage from '../assets/test_image.jpg';


const constraints = {
  video: { facingMode: 'environment' },
  audio: false,

};

const Guest = () => {
  const testImg = useRef();
  useEffect(() => {
    console.log('guest', testImage.current);
  }, []);

  const startCamera = () => {
    navigator.mediaDevices.getUserMedia(constraints).then((res) => {
      console.log('got cam?', res);
    });
  };

  return (
    <div className="guest">
      <h1>Guest Device</h1>
      <p>Enter Code or Use Camera to <span className="highlight" onClick={startCamera}>Scan</span></p>
      <input type="text" placeholder="Controller Code" />
      <img src={testImage} ref={testImg} />
    </div>
  );
};

export default Guest;
