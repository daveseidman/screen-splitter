import React from 'react';
import { Link } from 'react-router-dom';

import './Menu.scss';

const Menu = () => (
  <div className="menu">
    <h1>Screen Splitter</h1>
    <Link to="/host">Host a ScreenSplit</Link>
    <Link to="/guest">Join a ScreenSplit</Link>
  </div>
);

export default Menu;
