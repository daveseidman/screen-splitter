import React from 'react';
import './NotFound.scss';

const NotFound = () => (
  <div className="not-found">
    Page Not Found...
  </div>
);

export default NotFound;
