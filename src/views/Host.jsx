import { ArToolkitSource, ArToolkitContext, ArMarkerControls } from '@ar-js-org/ar.js/three.js/build/ar-threex.js';
import { TextureLoader, LinearFilter, Mesh, Scene, WebGLRenderer, PerspectiveCamera, Group, Vector3, Color, BoxGeometry, MeshBasicMaterial } from 'three';
import React, { useRef, useState, useEffect } from 'react';
import qrcode from 'qrcode-generator';
import './Host.scss';

const radToDeg = rad => rad * (180 / Math.PI);

const Host = (props) => {
  const svg = useRef();
  const { address } = props;
  const [link, setLink] = useState('');

  useEffect(() => {
    const renderer = new WebGLRenderer({ antialias: true, alpha: true });
    renderer.setPixelRatio(2);
    document.body.appendChild(renderer.domElement);

    const scene = new Scene();

    const camera = new PerspectiveCamera(50, 1280 / 720, 0.1, 100);
    scene.add(camera);

    let arToolkitSource;
    let arToolkitContext;
    const markers = [];
    const markerDebug = document.createElement('div');
    markerDebug.className = 'marker-debug';
    document.body.appendChild(markerDebug);
    const header = document.createElement('p');
    header.innerHTML = 'ID &nbsp; x &nbsp; y &nbsp; rot &nbsp; scl';
    markerDebug.appendChild(header);

    const getSourceOrientation = () => {
      if (!arToolkitSource) {
        return null;
      }

      if (arToolkitSource.domElement.videoWidth > arToolkitSource.domElement.videoHeight) {
        return 'landscape';
      }
      return 'portrait';
    };

    const initArContext = () => {
      arToolkitContext = new ArToolkitContext({
        cameraParametersUrl: `${ArToolkitContext.baseURL}../data/data/camera_para.dat`,
        detectionMode: 'color',
        labelingMode: 'black_region',
        patternRatio: 0.75,
        canvasWidth: 640,
        cavasHeight: 480,
        debug: true,
        imageSmoothingEnabled: true,
      });
      arToolkitContext.init(() => {
        camera.projectionMatrix.copy(arToolkitContext.getProjectionMatrix());
        arToolkitContext.arController.orientation = getSourceOrientation();
        arToolkitContext.arController.options.orientation = getSourceOrientation();

        console.log('arToolkitContext', arToolkitContext);
        window.arToolkitContext = arToolkitContext;

        for (let i = 0; i < 10; i += 1) {
          const markerRoot = new Group();
          scene.add(markerRoot);
          const markerObject = new Mesh(new BoxGeometry(1, 0.01, 1), new MeshBasicMaterial());
          markerObject.material.map = new TextureLoader().load(`/src/assets/pattern_${i}.png`);
          markerObject.material.map.minFilter = LinearFilter;
          markerObject.material.map.magFilter = LinearFilter;
          markerRoot.add(markerObject);
          const markerControl = new ArMarkerControls(arToolkitContext, markerRoot, {
            type: 'pattern',
            patternUrl: `/src/assets/pattern-marker_${i}.patt`,
            changeMatrixMode: 'modelViewMatrix',
            smooth: false,
            smoothCount: 5,
            smoothTolerance: 0.01,
            smoothThreshold: 4,
          });
          const markerEl = document.createElement('p');
          markerDebug.appendChild(markerEl);
          markers.push({
            el: markerEl,
            ar: markerControl,
          });
        }
      });
    };

    const onResize = () => {
      // arToolkitSource.onResizeElement();
      arToolkitSource.copyElementSizeTo(renderer.domElement);
      if (arToolkitContext.arController !== null) {
        arToolkitSource.copyElementSizeTo(arToolkitContext.arController.canvas);
      }
    };

    const render = () => {
      arToolkitContext.update(arToolkitSource.domElement);
      markers.forEach((marker, index) => {
        const { el } = marker;
        const { object3d } = marker.ar;
        const { x, y, z } = object3d.position;
        const xf = x > 0 ? `&nbsp;${x.toFixed(1)}` : x.toFixed(1);
        const yf = y > 0 ? `&nbsp;${y.toFixed(1)}` : y.toFixed(1);
        const r = radToDeg(object3d.rotation.y);
        const rf = r > 0 ? `&nbsp;${r.toFixed(1)}` : r.toFixed(1);
        const s = ((20 + z) * 2).toFixed(1);
        el.innerHTML = `${index}) ${object3d.visible ? `${xf},${yf}, ${rf}, ${s}` : '&nbsp;'}`;
      });
      renderer.render(scene, camera);
      requestAnimationFrame(render);
    };

    const start = () => {
      arToolkitSource = new ArToolkitSource({
        sourceType: 'webcam',
        sourceWidth: 640,
        sourceHeight: 480,
        displayWidth: 640,
        displayHeight: 480,
      });
      arToolkitSource.init(() => {
        initArContext();
        setTimeout(onResize, 500);
        render();
      });
    };

    window.addEventListener('resize', () => {
      onResize();
    });

    const buttons = document.createElement('div');
    buttons.className = 'buttons';

    const startButton = document.createElement('button');
    startButton.innerText = 'Start';
    startButton.addEventListener('click', start);

    const debugToggle = document.createElement('button');
    debugToggle.innerText = 'Toggle Debug';
    debugToggle.addEventListener('click', () => {
      const { canvas } = arToolkitContext.arController;
      canvas.classList[canvas.classList.contains('hidden') ? 'remove' : 'add']('hidden');
    });

    const markerToggle = document.createElement('button');
    markerToggle.innerText = 'Toggle Markers';
    markerToggle.addEventListener('click', () => {
      const { domElement } = renderer;
      domElement.classList[domElement.classList.contains('hidden') ? 'remove' : 'add']('hidden');
    });

    buttons.appendChild(startButton);
    buttons.appendChild(debugToggle);
    buttons.appendChild(markerToggle);
    document.body.appendChild(buttons);
  }, []);

  useEffect(() => {
    if (address.ip) {
      setLink(`${address.ip}:${address.port}/${address.room}`);
      const codeGenerator = qrcode(0, 'M');
      // codeGenerator.addData(`${location.origin}/${code}`);
      codeGenerator.addData(address.ip);
      codeGenerator.make();
      svg.current.innerHTML = codeGenerator.createSvgTag();
    }
  }, [address]);

  return (
    <div className="host">
      <h1>Host Device</h1>
      <p>Scan Code on Guest Devices</p>
      <div ref={svg} />
      <p>{link}</p>
    </div>
  );
};

export default Host;
