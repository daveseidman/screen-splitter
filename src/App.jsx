import React, { useEffect, useState } from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';

import { ToastContainer } from 'react-toastify';
import io from 'socket.io-client';
import NotFound from './views/NotFound';
import Menu from './views/Menu';
import Host from './views/Host';
import Guest from './views/Guest';
import useStore from './store';
import 'react-toastify/dist/ReactToastify.css';
import './App.scss';

const App = () => {
  const { getAll } = useStore();
  const [address, setAddress] = useState({});
  // TODO: lets revisit this, may not need lobbies or modules at this point
  // and after setting up the login and clientsList, we may not
  // even need experiences at this point
  useEffect(() => {
    const socket = io();
    socket.on('location', (e) => {
      console.log('Send this to host:', e);
      setAddress(e);
    });
  }, []);

  return (
    <Router>
      <div className="app">
        <Routes>
          <Route path="/" element={<Menu />} />
          <Route path="*" element={<NotFound />} />
          <Route path="/host/*" element={<Host address={address} />} />
          <Route path="/guest/*" element={<Guest />} />
        </Routes>
        <ToastContainer />
      </div>
    </Router>
  );
};

export default App;
